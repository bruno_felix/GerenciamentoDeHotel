=================================
Sistema de Gerenciamento de Hotel
=================================

===========
Tecnologias
===========
- Java EE
- Servlet 3.1
- JSP
- JavaScript
- Bootstrap

==============
Pr� requisitos
==============
- Tomcat v8.5
- Eclipse Neon J2EE

====================
Executar a aplica��o
====================
1. Import -> General -> Existing Projects into Workspace
2. Ir em Java Resources -> META-INF -> persistence.xml -> Alterar "username" e "password"
3. Executar arquivo "Script SQL"
4. Clicar com o bot�o direito no projeto -> Run as -> Run on Server
5. Login "admin" e Senha "admin"